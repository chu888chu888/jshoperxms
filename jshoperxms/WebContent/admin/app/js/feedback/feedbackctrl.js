define(['./module'],function(feedbackmodule){
	'use strict';

	/*=====End Of Page Element Directive=====*/
	
	/*=====Begin Of Save and Edit FeedbackT=====*/
	feedbackmodule.controller('feedback',ServerSaveFeedback);
	function ServerSaveFeedback($scope,$http,$location){
		$scope.gtparams=[];
		$scope.index=1;
		//通过location中的operate参数区分操作行为
		var operate=$location.search().operate;
		if(operate!=undefined&&operate=="save"){
			//控制保存按钮显示
			$scope.savebtn={show:true};
			$scope.title='添加反馈&参数';
			$scope.status="1";
			
			//保存数据方法
			$scope.save=function(){
				if(validate()){
					var content=$scope.content;
					var basicinfoid = $scope.basicinfoid;
					var feedbackpurpose = "1";
					if(basicinfoid==undefined||basicinfoid==""){
						feedbackpurpose="0";
					}
					var status=$scope.status;
					$http({
						method:'POST',
						url:'../mall/feedback/save.action',
						data:{
							'content':content,
							'basicinfoid':basicinfoid,
							'feedbackpurpose':feedbackpurpose,
							'status':status
						}
					}).
					success(function(data,status,headers,config){
						if(data.sucflag){
							$scope.info={
									show:true,
									msg:'反馈信息及参数添加成功'
							}
							$scope.errors={
									show:false
							}
						}
					}).
					error(function(data,status,headers,config){
						$scope.errors={
								show:true,
								msg:'系统异常'
						}
					});
				}
			}
		}
		
		//如果operate是edit则执行保存行为
		if(operate=='edit'){
			var id=$location.search().id;
			if(id!=undefined&&id!=""){
				$http({
					method:'POST',
					url:'../mall/feedback/find.action',
					data:{
						'feedbackId':id
					}
				}).
				success(function(data,status,headers,config){
					if(data.sucflag){
						$scope.title='正在对反馈数据进行编辑';
						$scope.content=data.bean.content;
						$scope.basicinfoid=data.bean.basicinfoid;
						$scope.status=data.bean.status;
						$scope.updatebtn={show:true};
					}
				}).
				error(function(data,status,headers,config){
					$scope.errors={
							show:true,
							msg:'系统异常'
					}
				});
				//更新数据方法
				$scope.update=function(){
					if(validate()){
						var content=$scope.content;
						var basicinfoid = $scope.basicinfoid;
						var feedbackpurpose = "1";
						if(basicinfoid==undefined||basicinfoid==""){
							feedbackpurpose="0";
						}
						var status=$scope.status;
						$http({
							method:'POST',
							url:'../mall/feedback/update.action',
							data:{
								'content':content,
								'basicinfoid':basicinfoid,
								'feedbackpurpose':feedbackpurpose,
								'status':status,
								'feedbackId':id
							}
						}).
						success(function(data,status,headers,config){
							if(data.sucflag){
								$scope.info={
										show:true,
										msg:'反馈数据及参数更新成功'
								}
							}
						}).
						error(function(data,status,headers,config){
							$scope.errors={
									show:true,
									msg:'系统异常'
							}
						});
					}
				}
				
			}
			
			
		}
		
		//表单字段验证
		function validate(){
			var content=$scope.content;
			if(content==undefined||content==""){
				$scope.errors={
						show:true,
						msg:"请输入反馈内容"
				}
				return false;
			}
			return true;
		}
	}
	/*=====End Of Save and Edit FeedbackT=====*/
	
	/*=====Begin Of Find List FeedbackT=====*/
	feedbackmodule.controller('feedbacklist',ServerFeedbackListCtrl);
	//查询列表数据
	function ServerFeedbackListCtrl($http,$location,$compile,$scope,$rootScope,$resource,DTOptionsBuilder,DTColumnBuilder,DTAjaxRenderer){
		var vm=this;
		vm.message='';
		vm.someClickHandler = someClickHandler;
		vm.selected={};
		vm.selectAll=false;
		vm.toggleAll=toggleAll;
		vm.toggleOne=toggleOne;
		vm.dtInstance={};
		var titleHtml='<input type="checkbox" ng-model="showCase.selectAll" ng-click="showCase.toggleAll(showCase.selectAll,showCase.selected)">';
		vm.dtOptions=DTOptionsBuilder.newOptions().withOption('ajax',{
			type:'POST',
			url:'../mall/feedback/findByPage.action',
			dataSrc:'data'
		})
		.withOption('processing',true)
		.withOption('paging',true)
		.withOption('serverSide',true)
		.withOption('createdRow',function(row,data,dataIndex){
			$compile(angular.element(row).contents())($scope);
		})
		.withOption('headerCallback', function(header) {
            if (!$scope.headerCompiled) {
                $scope.headerCompiled = true;
                $compile(angular.element(header).contents())($scope);
            }
        })
        .withOption('stateSave', true)
        .withOption('rowCallback',rowCallback)
		.withPaginationType('full_numbers')
		.withLanguageSource('./app/language/chinese.json')
		
		$scope.$on('handleRequest',function(){
			
		});
		$rootScope.getTableData=function serverData(){
			var req;
		}
		
		vm.dtColumns=[
		              DTColumnBuilder.newColumn(null).withTitle(titleHtml).notSortable().renderWith(function(data,type,full,meta){
		            	  vm.selected[full.id]=false;
		            	  return '<input type="checkbox" ng-model="showCase.selected['+data.id+']" ng-click="showCase.toggleOne(showCase.selected)">';
		              }),
		              DTColumnBuilder.newColumn('id').withTitle('ID').notVisible(),
			          DTColumnBuilder.newColumn('content').withTitle('内容').notSortable(),
			          DTColumnBuilder.newColumn('devicetype').withTitle('反馈客户端类型').notSortable(),
			          DTColumnBuilder.newColumn('feedbacksource').withTitle('反馈来源类型').notSortable(),
			          DTColumnBuilder.newColumn('feedbackpurpose').withTitle('反馈对象类型').notSortable(),
			          DTColumnBuilder.newColumn('basicinfoid').withTitle('商户ID').notSortable(),
			          DTColumnBuilder.newColumn('dataid').withTitle('反馈人').notSortable(),
			          DTColumnBuilder.newColumn('updatetime').withTitle('更新时间').notSortable(),
			          DTColumnBuilder.newColumn('status').withTitle('状态').notSortable(),
			          DTColumnBuilder.newColumn(null).withTitle('操作').notSortable().renderWith(actionHtml)];
		function actionHtml(data,type,full,meta){
			return '<button class="btn btn-warning" ng-click="edit('+data.id+')"><i class="fa fa-edit"></i></button>';
		}
		//表格中编辑按钮
		$scope.edit=function(id,name){
			$location.path('/feedback').search({'operate':'edit','id':id});
		}
		
		/**
		 * 跳转到添加商品类型和参数页面
		 */
		$scope.save=function(){
			$location.path('/feedback').search({'operate':'save'});
		}
		
		$scope.del=function(){
			var i=0;
			var ids=[];
			angular.forEach(vm.selected, function(data,index,array){
				if(data){
					i++;
					ids.push(index);
				}
			});
			if(i==0){
				$scope.errors={
						show:true,
						msg:'请选择一条记录'
				}
			}else{
				$scope.errors={
						show:false
				}
				//批量删除数据
				var idstrs=ids.join(",");
				$http({
					method:'POST',
					url:'../mall/feedback/del.action',
					data:{
						'ids':idstrs
					}
				}).
				success(function(data,status,headers,config){
					if(data.sucflag){
						$scope.info={
								show:true,
								msg:'批量删除成功'
						}
						//window.location.reload();
					}
					
				}).
				error(function(data,status,headers,config){
					$scope.errors={
							show:true,
							msg:'系统异常'
					}
				});
			}
		}

		/**
		 * 列表全选
		 */
		function toggleAll(selectAll,selectedItems){
			for(var id in selectedItems){
				if(selectedItems.hasOwnProperty(id)){
					selectedItems[id]=selectAll;
				}
			}
		}
		/**
		 * 列表单选
		 */
		function toggleOne(selectedItems){
			var me=this;
			for(var id in selectedItems){
				if(selectedItems.hasOwnProperty(id)){
					if(!selectedItems[id]){
						me.selectAll=false;
					}
				}
			}
			me.selectAll=true;
		}

		function someClickHandler(info) {
	        vm.message = info.id + ' - ' + info.name;
	    }
		/**
		 * 单击列表某行回调
		 */
		function rowCallback(nRow,aData,iDisplayIndex,iDisplayIndexFull){
			$('td', nRow).unbind('click');
	        $('td', nRow).bind('click', function() {
	            $scope.$apply(function() {
	                vm.someClickHandler(aData);
	            });
	        });
	        return nRow;
		}
		
	}
});