var mModule=angular.module("goodsModule",[]);

/**
 * 货物规格路由控制
 */
mModule.config(function($routeProvider){
	$routeProvider
	.when('/goodsment',{
		templateUrl:'../admin/app/goods/goodsment.html',
		controller:'goods'
	}).
	when('/savegoods',{
		templateUrl:'../admin/app/goods/goods.html',
		controller:'savegoods'
	});
});

mModule.controller('goods',['$scope','$http',function($scope,$http){
	$scope.title="商品列表";
}]);

mModule.controller('savegoods',['$scope','$http',function($scope,$http){
	$scope.title="保存商品";
}]);