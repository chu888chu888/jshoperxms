define(['angular','datatables','angular-resource'],function(ng){
	'use strict';
	return ng.module('membergroupmodule',['datatables','ngResource']);
});