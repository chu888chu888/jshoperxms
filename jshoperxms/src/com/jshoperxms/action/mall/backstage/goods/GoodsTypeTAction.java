package com.jshoperxms.action.mall.backstage.goods;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.InterceptorRefs;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.jshoperxms.action.mall.backstage.base.BaseTAction;
import com.jshoperxms.action.utils.BaseTools;
import com.jshoperxms.action.utils.enums.BaseEnums;
import com.jshoperxms.action.utils.enums.BaseEnums.DataUsingState;
import com.jshoperxms.action.utils.statickey.StaticKey;
import com.jshoperxms.entity.GoodsTypeT;
import com.jshoperxms.service.GoodsTypeTService;
import com.jshoperxms.service.impl.Serial;
import com.opensymphony.xwork2.ActionContext;

@Namespace("/mall/goods/goodstype")
@ParentPackage("jshoperxms")
@InterceptorRefs({ @InterceptorRef("mallstack") })
public class GoodsTypeTAction extends BaseTAction{
	@Resource
	private GoodsTypeTService goodsTypeTService;

	

	

	/**
	 * 检查是否有重名商品类型名称
	 * @return
	 */
	private boolean checkGoodsTypeName(){
		if(StringUtils.isBlank(this.getName())){
			return false;
		}
		String name=StringUtils.trim(this.getName());
		Criterion criterion=Restrictions.eq("name",name);
		GoodsTypeT goodsTypeT=this.goodsTypeTService.findOneByCriteria(GoodsTypeT.class, criterion);
		if(goodsTypeT!=null){
			return false;
		}
		return true;
	}
	
	
	/**
	 * 查询所有的商品类型
	 * @return
	 */
	@Action(value = "findAll", results = { @Result(name = "json", type = "json") })
	public String findAllGoodsTypeT(){
		Criterion criterion=Restrictions.eq("status", DataUsingState.USING.getState());
		List<GoodsTypeT>list=this.goodsTypeTService.findByCriteria(GoodsTypeT.class, criterion,Order.desc("updatetime"));
		if(!list.isEmpty()){
			beanlists=new ArrayList<GoodsTypeT>();
			beanlists=list;
			this.setSucflag(true);
		}
		return JSON;
	}
	
	/**
	 * 保存商品类型和参数
	 * @return
	 */
	@Action(value = "save", results = { @Result(name = "json", type = "json") })
	public String saveGoodsTypeT(){
		if(checkGoodsTypeName()){
			GoodsTypeT gtn=new GoodsTypeT();
			gtn.setGoodsTypeId(this.getSerial().Serialid(Serial.GOODSTYPE));
			gtn.setName(StringUtils.trim(this.getName()));
			gtn.setGoodsParameter(this.getGoodsparameter());
			gtn.setCreatetime(BaseTools.getSystemTime());
			gtn.setCreatorid(BaseTools.getAdminCreateId());
			gtn.setStatus(this.getStatus());
			gtn.setUpdatetime(BaseTools.getSystemTime());
			gtn.setVersiont(1);
			this.goodsTypeTService.save(gtn);
			this.setSucflag(true);
		}
		return JSON;
	}
	
	/**
	 * 获取所有商品类型 1，区分是否有排序，是否有搜索，然后进行相应的业务方法调用
	 * 
	 * @return
	 */
	@Action(value = "findByPage", results = { @Result(name = "json", type = "json") })
	public String findGoodsTypeTByPage() {
		Map<String,Object>params=ActionContext.getContext().getParameters();
//		for(Map.Entry<String, Object>entry:params.entrySet()){
//			String key=entry.getKey();
//			Object value=entry.getValue();
//			//System.out.println("key:"+ key +" value: "+Arrays.toString((String[])value));
//			
//		}
		//检测是否需要搜索内容
		
		findDefaultAllGoodsTypeT();

		return JSON;
	}

	public void processGoodsTypeTList(List<GoodsTypeT> list) {
		for (Iterator<GoodsTypeT> it = list.iterator(); it.hasNext();) {
			GoodsTypeT gtn = it.next();
			Map<String, Object> cellMap = new HashMap<String, Object>();
			cellMap.put("id", gtn.getGoodsTypeId());
			cellMap.put("name", gtn.getName());
			cellMap.put("status", BaseEnums.DataUsingState.getName(gtn.getStatus()));
			cellMap.put("updatetime", BaseTools.formateDbDate(gtn.getUpdatetime()));
			cellMap.put("version", gtn.getVersiont());
			data.add(cellMap);
		}
	}

	public void findDefaultAllGoodsTypeT() {
		int currentPage=1;
		if(this.getStart()!=0){
			currentPage=this.getStart()/this.getLength()==1?2:this.getStart()/this.getLength()+1;
		}
		int lineSize = this.getLength();
		Criterion criterion=Restrictions.ne("status", DataUsingState.DEL.getState());
		recordsFiltered=recordsTotal = this.goodsTypeTService.count(GoodsTypeT.class,criterion).intValue();
		List<GoodsTypeT> list = this.goodsTypeTService.findByCriteriaByPage(
				GoodsTypeT.class, criterion,Order.desc("updatetime"), currentPage,
				lineSize);
		this.processGoodsTypeTList(list);
	}
	
	
	/**
	 * 根据商品类型id查询内容
	 * @return
	 */
	@Action(value = "find", results = { @Result(name = "json", type = "json") })
	public String findOneGoodsTypeT(){
		if(StringUtils.isBlank(this.getGoodsTypeId())){
			return JSON;
		}
		GoodsTypeT goodsTypeT=this.goodsTypeTService.findByPK(GoodsTypeT.class, this.getGoodsTypeId());
		if(goodsTypeT!=null){
			bean=goodsTypeT;
			this.setSucflag(true);
		}
		return JSON;
	}
	
	/**
	 * 更新商品类型参数
	 * @return
	 */
	@Action(value = "update", results = { @Result(name = "json", type = "json") })
	public String updateGoodsTypeT(){
		if(StringUtils.isBlank(this.getGoodsTypeId())){
			return JSON;
		}
		GoodsTypeT gtn=this.goodsTypeTService.findByPK(GoodsTypeT.class, this.getGoodsTypeId());
		gtn.setName(StringUtils.trim(this.getName()));
		gtn.setGoodsParameter(this.getGoodsparameter());
		gtn.setStatus(this.getStatus());
		gtn.setCreatorid(BaseTools.getAdminCreateId());
		gtn.setUpdatetime(BaseTools.getSystemTime());
		gtn.setVersiont(gtn.getVersiont()+1);
		this.goodsTypeTService.update(gtn);
		this.setSucflag(true);
		return JSON;
	}

	/**
	 * 删除商品类型参数
	 * @return
	 */
	@Action(value = "del", results = { @Result(name = "json", type = "json") })
	public String delGoodsTypeT(){
		String ids[]=StringUtils.split(this.getIds(), StaticKey.SPLITDOT);
		for(String s:ids){
			GoodsTypeT goodsTypeT=this.goodsTypeTService.findByPK(GoodsTypeT.class, s);
			if(goodsTypeT!=null){
				goodsTypeT.setStatus(DataUsingState.DEL.getState());
				goodsTypeT.setUpdatetime(BaseTools.getSystemTime());
				this.goodsTypeTService.update(goodsTypeT);
			}
		}
		this.setSucflag(true);
		return JSON;
	}
	
	/**
	 * 批量删除的内容串
	 */
	private String ids;
	private String status;
	private String goodsTypeId;
	private String name;
	private String goodsparameter;
	private GoodsTypeT bean;
	private List<GoodsTypeT>beanlists;
	private List<Map<String, Object>> data = new ArrayList<Map<String, Object>>();
	private int recordsTotal = 0;
	private int recordsFiltered=0;
	/**
	 * 表格控件请求次数
	 */
	private int draw;
	/**
	 * 请求第几页
	 */
	private int start;
	/**
	 * 请求几条
	 */
	private int length;
	private boolean sucflag;

	public List<GoodsTypeT> getBeanlists() {
		return beanlists;
	}



	public void setBeanlists(List<GoodsTypeT> beanlists) {
		this.beanlists = beanlists;
	}



	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}

	public GoodsTypeT getBean() {
		return bean;
	}

	public void setBean(GoodsTypeT bean) {
		this.bean = bean;
	}

	public String getGoodsparameter() {
		return goodsparameter;
	}

	public void setGoodsparameter(String goodsparameter) {
		this.goodsparameter = goodsparameter;
	}

	public String getGoodsTypeId() {
		return goodsTypeId;
	}

	public void setGoodsTypeId(String goodsTypeId) {
		this.goodsTypeId = goodsTypeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Map<String, Object>> getData() {
		return data;
	}

	public void setData(List<Map<String, Object>> data) {
		this.data = data;
	}



	public int getRecordsTotal() {
		return recordsTotal;
	}

	public void setRecordsTotal(int recordsTotal) {
		this.recordsTotal = recordsTotal;
	}

	public int getRecordsFiltered() {
		return recordsFiltered;
	}

	public void setRecordsFiltered(int recordsFiltered) {
		this.recordsFiltered = recordsFiltered;
	}

	public int getDraw() {
		return draw;
	}

	public void setDraw(int draw) {
		this.draw = draw;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public boolean isSucflag() {
		return sucflag;
	}

	public void setSucflag(boolean sucflag) {
		this.sucflag = sucflag;
	}


	
}
