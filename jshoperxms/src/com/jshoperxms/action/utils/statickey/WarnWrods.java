package com.jshoperxms.action.utils.statickey;
/**
 * 系统返回的警告信息
* @ClassName: WarnWrods 
* @Description: 集合了所有系统返回的警告信息
* @author jcchen
* @date 2015年8月31日 上午10:37:37 
*
 */
public class WarnWrods {

	public static String EMPTY_LOGINNAME="请输入登陆账号";
	
	public static String EMPTY_LOGINPWD="请输入登陆密码";
	
	public static String EMPTY_RECORD="没有找到任何结果";
	
	public static String EMPTY_ACCOUNT="该账号登陆失败";
	
}
