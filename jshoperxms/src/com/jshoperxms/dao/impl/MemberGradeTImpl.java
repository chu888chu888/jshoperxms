package com.jshoperxms.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.jshoperxms.dao.MemberGradeTDao;
import com.jshoperxms.entity.MemberGradeT;


@Repository("memberGradeTDao")
public class MemberGradeTImpl extends BaseTDaoImpl<MemberGradeT> implements MemberGradeTDao {
    private static final Logger log = LoggerFactory.getLogger(MemberGradeTImpl.class);
	
	
}