package com.jshoperxms.service;


import com.jshoperxms.entity.GoodsTypeBrandT;

public interface GoodsTypeBrandTService extends BaseTService<GoodsTypeBrandT> {

	/**
	 * 添加商品品牌类型信息
	 * @param gtbv
	 * @param goodsTypeId
	 * @param goodsTypeName
	 */
	public void saveGoodsTypeBrands(String brands,String goodsTypeId,String goodsTypeName);
}
