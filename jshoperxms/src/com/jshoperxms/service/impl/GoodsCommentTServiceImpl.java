package com.jshoperxms.service.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.jshoperxms.entity.GoodsCommentT;
import com.jshoperxms.service.GoodsCommentTService;

@Service("goodsCommentTService")
@Scope("prototype")
public class GoodsCommentTServiceImpl extends BaseTServiceImpl<GoodsCommentT>implements GoodsCommentTService {

}
