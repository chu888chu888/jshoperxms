package com.jshoperxms.service.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.jshoperxms.entity.SystemMailT;
import com.jshoperxms.service.SystemMailTService;

@Service("systemMailTService")
@Scope("prototype")
public class SystemMailTServiceImpl extends BaseTServiceImpl<SystemMailT>implements SystemMailTService {



}
